__author__ = 'Bananonina'

import MapReduce
import sys
"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    key = record[0]
    value = record[1]
    mr.emit_intermediate(key, value)

def reducer(key, list_of_values):
    for v in list_of_values:
        try:
            if key not in mr.intermediate[v]:
                mr.emit((key ,v))
                mr.emit((v, key))
        except KeyError:
            for x in mr.intermediate.keys():
                if v in mr.intermediate[x]:
                    mr.emit((x,v))
                    mr.emit((v,x ))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
