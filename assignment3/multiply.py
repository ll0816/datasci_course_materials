__author__ = 'Bananonina'


import MapReduce
import sys
"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    key =record[1:3]
    if record[0]=='a':
        for i in range(0,5):
            value={key[1]:record[3]}
            mr.emit_intermediate((key[0],i),value)
    else:
        for j in range(0,5):
            value={key[0]+5:record[3]}
            mr.emit_intermediate((j,key[1]),value)

def reducer(key, list_of_values):
    dict={}
    product=0
    for d in list_of_values:
        dict[d.keys()[0]]=d.values()[0]
    for k in range(0,5):
        product+=dict.get(k,0)*dict.get(k+5,0)
    mr.emit(key+(product,))



# Do not modify below this line
# =============================
if __name__ == '__main__':
    inputdata = open(sys.argv[1])
    mr.execute(inputdata, mapper, reducer)
