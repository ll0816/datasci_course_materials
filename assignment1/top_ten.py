import sys
import json
def main():
    with open(sys.argv[1]) as tweet:
        freq = {}
        for line in tweet:
            j = json.loads(line)
            try:
                text= j['entities']['hashtags']
                text=text[0]['text'].encode('utf-8').split()
                text=text[0]
                if text in freq.keys():
                    freq[text]+=1
                else :
                    freq[text]=1
            except IndexError:
                text = 'InvalidTweet'
            except TypeError:
                text = 'InvalidTweet'
            except UnicodeDecodeError:
                text = 'InvalidTweet'
            except KeyError:
                text = 'InvalidTweet'
        sort=sorted(freq,key=freq.get,reverse=True)
        for i in range(10):
            print sort[i],freq[sort[i]]

if __name__ == '__main__':
    main()
