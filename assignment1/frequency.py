import sys
import json


def main():
    with open(sys.argv[1]) as tweet:
        freq = {}
        count=0
        for line in tweet:
            s = 0
            jline = json.loads(line)
            try:
                j = jline['text']
                text = j.encode('utf-8').lower()
                text = text.split()
            except UnicodeDecodeError:
                text = 'InvalidTweet'
            except KeyError:
                text = 'InvalidTweet'
            term_in_line = []
            if text != 'InvalidTweet':
                for t in text:
                    term_in_line.append(t)
            else:
                pass
            for i in term_in_line:
                if i in freq.keys():
                    freq[i] += 1
                    count+=1
                else:
                    freq[i] = 1
                    count+=1
        for x in freq.keys():
            print x, float(freq[x]) / float(count)


if __name__ == '__main__':
    main()
