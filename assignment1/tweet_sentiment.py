
import sys
import json


def lines(fp):
    print str(len(fp.readlines()))


def main():
    with open(sys.argv[1]) as afinnfile:
        scores = {}
        for line in afinnfile:
            term, score = line.split("\t")
            scores[term] = int(score)
    with open(sys.argv[2]) as tweet:
        for line in tweet:
            s = 0
            jline = json.loads(line)
            for i in jline:
                text = i.encode('utf-8').lower().split()
                text = text[0]
                if text in scores.keys():
                    s += scores[text]
                else:
                    pass
            print s

if __name__ == '__main__':
    main()
