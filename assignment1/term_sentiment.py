import sys
import json


def lines(fp):
    print str(len(fp.readlines()))


def main():
    with open(sys.argv[1]) as afinnfile:
        scores = {}
        for line in afinnfile:
            term, score = line.split("\t")
            scores[term] = int(score)
    with open(sys.argv[2]) as tweet:
        term={}
        freq={}
        for line in tweet:
            s = 0
            jline = json.loads(line)
            try:
                j=jline['text']
                text=j.encode('utf-8').lower().split()
            except UnicodeDecodeError:
                text='InvalidTweet'
            except KeyError:
                text='InvalidTweet'
            term_in_line=[]
            if text!='InvalidTweet':
                for t in text:
                     if t in scores.keys():
                         s += scores[t]
                     else:
                         term_in_line.append(t)
            else: pass
            for i in term_in_line:
                if i in term.keys():
                    term[i]+=s
                    freq[i]+=1
                else:
                    term[i]=s
                    freq[i]=1
        for x in term.keys():
            print x, float(term[x])/float(freq[x])


if __name__ == '__main__':
    main()