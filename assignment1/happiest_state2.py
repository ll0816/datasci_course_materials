import sys,json

states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}



def main():
    with open(sys.argv[1]) as afinnfile:
        scores = {}
        for line in afinnfile:
            term, score = line.split("\t")
            scores[term] = int(score)

    with open(sys.argv[2]) as tweet:
        happy_state={}
        count={}
        for line in tweet:
            s = 0
            jline = json.loads(line)
            try:
                j=jline['text']
                text=j.encode('utf-8').lower().split()
            except UnicodeDecodeError:
                text = 'InvalidTweet'
            except KeyError:
                text='InvalidTweet'
            if text!='InvalidTweet':
                for t in text:
                    if t in scores.keys():
                        s += scores[t]
                try :
                    state=jline['place']['full_name']
                    state=state.encode('utf-8').split(",")[0]
                except TypeError:
                   state = 'InvalidState'
                except UnicodeDecodeError:
                    state = 'InvalidState'
                except KeyError:
                    state = 'InvalidState'

                if state!='InvalidState':
                    if len(state)==1:
                        abbr=state[0:2].upper()
                    else :
                        abbr=state[0][0]+state[1][0]
                        abbr=abbr.upper()
                    if abbr in states.keys():
                        if abbr in happy_state.keys():
                            happy_state[abbr]+=s
                            count[abbr]+=1
                        else:
                            happy_state[abbr]=s
                            count[abbr]=1
        for key in happy_state.keys():
            happy_state[key]=float(happy_state[key])/float(count[key])
        m=0
        n=''
        for name,value in happy_state.items() :
            if value>m:
                n=name
        print str(n)


if __name__ == '__main__':
    main()