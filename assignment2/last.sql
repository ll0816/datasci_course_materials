select max(v)from
(
select a.docid,frequency.docid,sum(a.count*frequency.count) as v 
 from frequency join a on (a.term=frequency.term)
  where a.docid>frequency.docid
   group by a.docid,frequency.docid
);